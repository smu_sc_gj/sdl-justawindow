/*
  Based on http://lazyfoo.net/SDL_tutorials/
  Used with permission. 
*/

//Include SDL library
#include "SDL/SDL.h"

int main( int argc, char* args[] )
{
    //Declare pointers to surface structures
    SDL_Surface* screen = NULL;

    //Initialise SDL
    //NOTE: SDL_INIT_EVERYTHING is a constant defined by SDL
    SDL_Init( SDL_INIT_EVERYTHING );

    //Surface structure we'll use for the window - screen
    //This is essentially the back buffer. 
    screen = SDL_SetVideoMode( 640, 480, 32, SDL_SWSURFACE );

    //Refresh the screen (replace with backbuffer. 
    SDL_Flip( screen );

    //Pause to allow the image to be seen
    SDL_Delay( 10000 );

    //Shutdown SDL - clear up resorces etc. 
    SDL_Quit();

    return 0;
}
